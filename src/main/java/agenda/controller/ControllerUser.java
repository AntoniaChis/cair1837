package agenda.controller;

import agenda.model.base.User;
import agenda.model.repository.classes.RepositoryUserFile;
import agenda.model.repository.interfaces.RepositoryUser;
import java.util.List;

public class ControllerUser {
    private RepositoryUser userRep;

    public ControllerUser(){
        try{
           this.userRep = new RepositoryUserFile();
        }catch (Exception e) {

        }
    }

    public User getByUsername(String username) {
        return this.userRep.getByUsername(username);
    }

    public User getByName(String name) {
        return this.userRep.getByName(name);
    }

    public boolean changePasswd(User user, String oldPasswd, String newPasswd) {
        return this.userRep.changePasswd(user,oldPasswd,newPasswd);
    }

    public boolean save() {
        return userRep.save();
    }

    public List<User> getUsers() {
        return this.userRep.getUsers();
    }

    public int getCount() {
        return this.userRep.getCount();
    }
}
