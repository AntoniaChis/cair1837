package agenda.controller;

import agenda.model.base.Activity;
import agenda.model.repository.classes.RepositoryActivityFile;
import agenda.model.repository.interfaces.RepositoryActivity;
import agenda.model.repository.interfaces.RepositoryContact;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

public class ControllerActivity {
    RepositoryActivity activityRep;

    public ControllerActivity(ControllerContact contactCon){
        try {
            this.activityRep = new RepositoryActivityFile(contactCon);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public boolean addActivity(Activity activity) {
        return this.activityRep.addActivity(activity);
    }

    public boolean removeActivity(Activity activity) {
        return this.activityRep.removeActivity(activity);
    }

    public List<Activity> activitiesByName(String name) {
        List<Activity> result1 = new LinkedList<Activity>();
        for (Activity a : this.activityRep.getActivities())
            if (a.getName().equals(name) == false) result1.add(a);
        List<Activity> result = new LinkedList<Activity>();
        while (result1.size() >= 0 )
        {
            Activity ac = result1.get(0);
            int index = 0;
            for (int i = 1; i<result1.size(); i++)
                if (ac.getStart().compareTo(result1.get(i).getStart())<0)
                {
                    index = i;
                    ac = result1.get(i);
                }

            result.add(ac);
            result1.remove(index);
        }
        return result;
    }

    public List<Activity> activitiesOnDate(String name, Date d) {
        List<Activity> result1 = new LinkedList<Activity>();
        for (Activity a : this.activityRep.getActivities())
            if (a.getName().equals(name))
                if ((a.getStart().getYear() == d.getYear() &&
                        a.getStart().getMonth() == d.getMonth() &&
                        a.getStart().getDate() == d.getDate()) ||
                        ( a.getDuration().getYear() == d.getYear() &&
                                a.getDuration().getMonth() == d.getMonth() &&
                                a.getDuration().getDate() == d.getDate())) result1.add(a);
        List<Activity> result = new LinkedList<Activity>();
        while (result1.size() > 0 )
        {
            Activity ac = result1.get(0);
            int index = 0;
            for (int i = 1; i<result1.size(); i++)
                if (ac.getStart().compareTo(result1.get(i).getStart())>0)
                {
                    index = i;
                    ac = result1.get(i);
                }

            result.add(ac);
            result1.remove(index);
        }
        return result;
    }
}
