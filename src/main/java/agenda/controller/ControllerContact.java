package agenda.controller;
import agenda.exceptions.InvalidFormatException;
import agenda.model.base.Contact;
import agenda.model.repository.classes.RepositoryContactFile;
import agenda.model.repository.interfaces.RepositoryContact;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.util.LinkedList;
import java.util.List;

public class ControllerContact {
    RepositoryContact contactRep;

    public ControllerContact(){
        try {
            this.contactRep = new RepositoryContactFile();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public RepositoryContact getContactRep() {
        return contactRep;
    }

    public List<Contact> getContacts() {
        return this.contactRep.getContacts();
    }

    public void addContact(String nume, String adresa, String phone, String email) {
        try{
            Contact contact = new Contact(nume,adresa,phone,email);
            this.contactRep.addContact(contact);
        }catch (InvalidFormatException e) {
            if (e.getCause() != null)
                System.out.printf("Eroare: %s - %s\n" + e.getMessage(), e
                        .getCause().getMessage());
            else
                System.out.printf("Eroare: %s\n" + e.getMessage());
        }
    }

    public boolean removeContact(Contact contact) {
        return contactRep.removeContact(contact);
    }

    public Contact getByName(String string) {
        for (Contact c : this.contactRep.getContacts())
            if (c.getName().equals(string))
                return c;
        return null;
    }
}
